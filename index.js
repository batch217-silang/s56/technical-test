function countLetter(letter, sentence) {
    let result = 0;

    if( result  === 0 ) {
            return undefined;
        }
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    
    for (let i = 0; i < letter.length; i++) {
      if (letter.charAt(i) == sentence) {
            result += 1;
        }
    }
    return result;
}
const sentence = ('Helllllllo Worlllllld');
const letterToCheck = ('l');
const totalLetters = countLetter(sentence, letterToCheck);
console.log(totalLetters);
 
/****************************TEST2****************************/
function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

 return text.split('').filter((item, pos, arr)=> arr.indexOf(item) == pos).length == text.length;
}
console.log(isIsogram('Hello'));
console.log(isIsogram('World'));



/****************************TEST3****************************/
function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a lettering.

    for (let age = 0; age <= age.length; age++){
    if (age < 13){
        return undefined;
        continue; 
    }
    
    if (age % 5 === 0){
        continue; 
    }
    console.log(age);
    
    if (age <= 50){
        break;
    }
}

/****************************TEST4****************************/
const items = [
        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    ];

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let newArr = [];    
     for(let i = 0; i < items.length; i++){
       if(items[i].stocks === 0){
         newArr.push(items[i].category)
       }
    }
    return [...new Set(newArr)];
  }
  
console.log(findHotCategories(items));

/****************************TEST5****************************/
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

   const res = [];
   for(let i = 0; i < candidateA.length; i++){
      if(!candidateB.includes(candidateA[i])){
         continue;
      };
      res.push(candidateA[i]);
   };
   return res;
};


function findVoters (...arrs) {
   let res = arrs[0].slice();
   for(let i = 1; i < arrs.length; i++){
      res = findFlyingVoters(res, arrs[i]);
   };
   return res;
};
console.log(findVoters(candidateA, candidateB));




module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};